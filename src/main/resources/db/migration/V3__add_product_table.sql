CREATE TABLE product
(
    id          UUID NOT NULL,
    name        VARCHAR(255),
    customer_id UUID,
    CONSTRAINT pk_product PRIMARY KEY (id)
);

ALTER TABLE product
    ADD CONSTRAINT FK_PRODUCT_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES customer (id);