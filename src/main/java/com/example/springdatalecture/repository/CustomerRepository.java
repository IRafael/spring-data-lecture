package com.example.springdatalecture.repository;

import com.example.springdatalecture.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, UUID> {

    Optional<Customer> findCustomerByName(String name);

}