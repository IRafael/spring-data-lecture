package com.example.springdatalecture;

import com.example.springdatalecture.entity.Product;
import com.example.springdatalecture.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class SpringDataLectureApplication implements CommandLineRunner {

    private final CustomerService customerService;

    public static void main(String[] args) {
        SpringApplication.run(SpringDataLectureApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Joining thread, you can press Ctrl+C to shutdown application");

        customerService.createCustomer("Rafael", "toy", "game", "box");
        customerService.createCustomer("Ivan", "food", "fruit", "water");
        customerService.createCustomer("Danil", "display", "laptop");


        for (final Product product : customerService.findAllCustomerProducts("Rafael")) {
            log.info(product.getName());
        }
        log.info("---");
        for (final Product product : customerService.findAllProducts()) {
            log.info(product.getName());
        }
        log.info("---");
        for (final Product product : customerService.findProductByPageAndAmount(3, 2)) {
            log.info(product.getName());
        }
        log.info("---");
        for (final Product product : customerService.findProductByPageAndAmountWithDESCSortByName(1, 5)) {
            log.info(product.getName());
        }

        Thread.currentThread().join();
    }

}
