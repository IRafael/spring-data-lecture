package com.example.springdatalecture.service;

import com.example.springdatalecture.entity.Customer;
import com.example.springdatalecture.entity.Product;
import com.example.springdatalecture.repository.CustomerRepository;
import com.example.springdatalecture.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;


@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;

    @Transactional
    public void createCustomer(final String name, final String... products) {
        final Customer customer = new Customer();
        customer.setName(name);
        customerRepository.save(customer);

        for (final String productName : products) {
            final Product product = new Product();
            product.setName(productName);
            product.setCustomer(customer);

            productRepository.save(product);
        }
    }

    @Transactional
    public Collection<Product> findAllCustomerProducts(final String name) {
        final Optional<Customer> customerByName = customerRepository.findCustomerByName(name);
        if (customerByName.isPresent()) {
            Hibernate.initialize(customerByName.get().getProducts());
            return customerByName.get().getProducts();
        } else {
            return Collections.emptyList();
        }
    }

    public Collection<Product> findAllProducts() {
        return productRepository.findAll(Sort.by("name"));
    }

    public Collection<Product> findProductByPageAndAmount(final int page, final int amount) {
        return productRepository.findAll(PageRequest.of(page, amount)).getContent();
    }

    public Collection<Product> findProductByPageAndAmountWithDESCSortByName(final int page, final int amount) {
        return productRepository.findAll(PageRequest.of(page, amount, Sort.by("name").descending())).getContent();
    }
}
